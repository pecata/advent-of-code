package main

import (
	"os"
	"strings"
)

func isVisible(forest []string, rowIndex int, colIndex int) bool {
	treeSize := forest[rowIndex][colIndex]

	// from top
	visible := true
	for r := 0; r < rowIndex; r++ {
		visible = visible && forest[r][colIndex] < treeSize
	}

	if visible {
		return true
	}

	// from right
	visible = true
	for c := len(forest[rowIndex]) - 1; c > colIndex; c-- {
		visible = visible && forest[rowIndex][c] < treeSize
	}

	if visible {
		return true
	}

	// from bottom
	visible = true
	for r := len(forest) - 1; r > rowIndex; r-- {
		visible = visible && forest[r][colIndex] < treeSize
	}

	if visible {
		return true
	}

	// from left
	visible = true
	for c := 0; c < colIndex; c++ {
		visible = visible && forest[rowIndex][c] < treeSize
	}

	return visible
}

func getScenicScore(forest []string, rowIndex int, colIndex int) int {
	treeSize := forest[rowIndex][colIndex]
	rowSize := len(forest)
	colSize := len(forest[rowIndex])
	score := 1

	// to top
	count := 0
	for r := rowIndex - 1; r >= 0; r-- {
		count++
		if forest[r][colIndex] >= treeSize {
			break
		}
	}
	score *= count

	// to right
	count = 0
	for c := colIndex + 1; c < colSize; c++ {
		count++
		if forest[rowIndex][c] >= treeSize {
			break
		}
	}
	score *= count

	// to bottom
	count = 0
	for r := rowIndex + 1; r < rowSize; r++ {
		count++
		if forest[r][colIndex] >= treeSize {
			break
		}
	}
	score *= count

	// to left
	count = 0
	for c := colIndex - 1; c >= 0; c-- {
		count++
		if forest[rowIndex][c] >= treeSize {
			break
		}
	}
	score *= count

	return score
}

func partOne(forest []string) {
	count := 0
	for ronIndex, row := range forest {
		for colIndex, _ := range row {
			if isVisible(forest, ronIndex, colIndex) {
				count++
			}
		}
	}

	println(count)
}

func partTwo(forest []string) {
	max := 0
	for ronIndex, row := range forest {
		for colIndex, _ := range row {
			scenicScore := getScenicScore(forest, ronIndex, colIndex)
			if scenicScore > max {
				max = scenicScore
			}
		}
	}

	println(max)
}

func main() {
	input, _ := os.ReadFile("in")
	forest := strings.Split(string(input), "\n")

	partOne(forest)
	partTwo(forest)
}
