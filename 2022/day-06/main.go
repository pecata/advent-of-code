package main

import (
	"os"
)

const (
	partOne = 4
	partTwo = 14
)

type circularBuffer struct {
	index int
	size  int
	data  []rune
	hash  map[rune]int
}

func constructBuffer(size int) *circularBuffer {
	return &circularBuffer{
		index: 0,
		size:  size,
		data:  make([]rune, size),
		hash:  map[rune]int{},
	}
}

func (c *circularBuffer) add(r rune) bool {
	c.index = (c.index + 1) % c.size
	out := c.data[c.index]
	c.data[c.index] = r

	c.hash[out] = c.hash[out] - 1
	c.hash[r] = c.hash[r] + 1

	count := 0
	for _, v := range c.hash {
		if v > 1 {
			count++
		}
	}
	return count == 0
}

func main() {
	input, _ := os.ReadFile("in")
	s := string(input)

	size := partTwo
	buffer := constructBuffer(size)
	res := 0
	for i, r := range s {
		b := buffer.add(r)
		if i >= size && b {
			res = i
			break
		}
	}

	println(res + 1)
}
