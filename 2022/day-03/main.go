package main

import (
	"os"
	"strings"
)

func getPriority(r uint8) uint {
	if 'a' <= r && r <= 'z' {
		return uint(r - 'a' + 1)
	}
	return uint(r - 'A' + 27)
}

func partOne(backpacks []string) uint {
	sum := uint(0)
	for _, backpack := range backpacks {
		l := len(backpack) / 2
		m := map[uint8]int{}
		for i := 0; i < l; i++ {
			m[backpack[i]] = m[backpack[i]] + 1
		}
		for i := 0; i < l; i++ {
			r := backpack[l+i]
			if m[r] > 0 {
				sum += getPriority(r)
				break
			}
		}
	}
	return sum
}

func partTwo(backpacks []string) uint {
	sum := uint(0)
	m := map[uint8]int{}
	for bi, backpack := range backpacks {
		bi3 := bi % 3
		if bi3 == 0 {
			m = map[uint8]int{}
		}

		for _, r := range backpack {
			ri := uint8(r)
			if bi3 == 0 {
				m[ri] = 1
			} else if bi3 == 1 && m[ri] <= 1 {
				m[ri] = m[ri] + 2
			} else if bi3 == 2 && m[ri] == 3 {
				sum += getPriority(ri)
				break
			}
		}
	}
	return sum
}

func main() {
	input, _ := os.ReadFile("in")
	backpacks := strings.Split(string(input), "\n")

	sum := partTwo(backpacks)

	println(sum)
}
