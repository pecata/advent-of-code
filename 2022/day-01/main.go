package main

import (
	"os"
	"sort"
	"strconv"
	"strings"
)

const (
	partOne = 1
	partTwo = 3
)

func main() {
	input, _ := os.ReadFile("in")
	split := strings.Split(string(input), "\n\n") // separate crates from commands

	var data []int
	for _, elfData := range split {
		sum := 0
		snacks := strings.Split(elfData, "\n")
		for _, calories := range snacks {
			c, _ := strconv.Atoi(calories)
			sum += c
		}

		data = append(data, sum)
	}

	sort.Ints(data)

	l := len(data)
	sum := 0
	for i := 0; i < partOne; i++ {
		sum += data[l-1-i]
	}
	println(sum)
}
