package main

import (
	"fmt"
	"os"
	"strings"
)

type stack struct {
	data []string
}

func (s *stack) push(el string) {
	s.data = append(s.data, el)
}

func (s *stack) pushAll(el []string) {
	s.data = append(s.data, el...)
}

func (s *stack) pop(n int) (el []string) {
	i := len(s.data) - n
	el = s.data[i:]
	s.data = s.data[:i]
	return
}

func (s *stack) top() string {
	l := len(s.data)
	if l == 0 {
		return ""
	}
	return s.data[l-1]
}

func getStackNames(deckKeys string) []string {
	buff := ""
	var names []string
	for _, l := range deckKeys {
		if l != ' ' {
			buff += string(l)
			continue
		}

		if len(buff) > 0 {
			names = append(names, buff)
		}
		buff = ""
	}

	if len(buff) > 0 {
		names = append(names, buff)
	}

	return names
}

func getStackSeparatorColumnIndexes(bottomRow string, numberOfStacks int) []int {
	// this method assumes that all stack have containers on the given row
	isInContainer := true
	columnIndexes := make([]int, numberOfStacks-1)
	filledIndex := 0
	for ind, l := range bottomRow {
		isSpace := l == ' '
		if isSpace && isInContainer {
			columnIndexes[filledIndex] = ind
			filledIndex++
			isInContainer = false
		} else if !isSpace && !isInContainer {
			isInContainer = true
		}
	}
	return columnIndexes
}

func parseData(crates []string) ([]stack, []string) {
	stackNames := getStackNames(crates[len(crates)-1])
	numberOfStacks := len(stackNames)
	fmt.Println(stackNames)
	stackSeparatorIndexes := getStackSeparatorColumnIndexes(crates[len(crates)-2], numberOfStacks)

	data := make([]stack, numberOfStacks)
	for crateInd := len(crates) - 2; crateInd >= 0; crateInd-- {
		isIn := false
		buff := ""
		for letterIndex, letter := range crates[crateInd] {
			if letter == '[' {
				isIn = true
				buff = ""
				continue
			}

			if letter == ']' {
				isIn = false

				stackIndex := 0
				for i := len(stackSeparatorIndexes) - 1; i >= 0; i-- {
					if stackSeparatorIndexes[i] < letterIndex {
						stackIndex = i + 1
						break
					}
				}
				data[stackIndex].push(buff)

				continue
			}

			if isIn {
				buff = buff + string(letter)
			}
		}
	}

	return data, stackNames
}

func executeCommands(data []stack, stackNamesSlice []string, commands []string) []stack {
	stackNames := map[string]int{}
	for i, s := range stackNamesSlice {
		stackNames[s] = i
	}

	for _, command := range commands {
		var qty int
		var from, to string
		_, _ = fmt.Sscanf(command, "move %d from %s to %s", &qty, &from, &to)

		fromSI := stackNames[from]
		toSI := stackNames[to]

		// region Part One
		//for i := 0; i < qty; i++ {
		//	el := data[fromSI].pop(1)
		//	data[toSI].pushAll(el)
		//}
		// endregion

		// region Part Two
		el := data[fromSI].pop(qty)
		data[toSI].pushAll(el)
		// endregion

	}

	return data
}

func serializeTop(data []stack) string {
	res := ""
	for _, s := range data {
		res += s.top()
	}
	return res
}

func main() {
	input, _ := os.ReadFile("in")
	split := strings.Split(string(input), "\n\n") // separate crates from commands

	data, stackNames := parseData(strings.Split(split[0], "\n"))
	data = executeCommands(data, stackNames, strings.Split(split[1], "\n"))
	println(serializeTop(data))
}
