package main

import (
	"os"
	"strings"
)

type value struct {
	v int
	e bool
}

type record struct {
	a    value
	b    value
	aLtB value
}

var (
	empty = value{0, false}

	win  = value{6, true}
	draw = value{3, true}
	lose = value{0, true}

	rock     = value{1, true}
	paper    = value{2, true}
	scissors = value{3, true}

	rules = []record{
		{rock, rock, draw},
		{rock, paper, win},
		{rock, scissors, lose},
		{paper, rock, lose},
		{paper, paper, draw},
		{paper, scissors, win},
		{scissors, rock, win},
		{scissors, paper, lose},
		{scissors, scissors, draw},
	}
)

func findGame(i record) record {
	for _, r := range rules {
		if (!i.a.e || i.a == r.a) && (!i.b.e || i.b == r.b) && (!i.aLtB.e || i.aLtB == r.aLtB) {
			return r
		}
	}
	panic("Not found")
}

func determineShape(r rune) value {
	switch r {
	case 'A', 'X':
		return rock
	case 'B', 'Y':
		return paper
	case 'C', 'Z':
		return scissors
	}
	panic("Unknown input:" + string(r))
}

// region Part One
func calculateGamePointsPartOne(row string) int {
	game := findGame(record{determineShape(rune(row[0])), determineShape(rune(row[2])), empty})
	return game.b.v + game.aLtB.v
}

// endregion

// region Part Two
func determineEndOfGame(r rune) value {
	switch r {
	case 'X':
		return lose
	case 'Y':
		return draw
	case 'Z':
		return win
	}
	panic("Unknown input:" + string(r))
}

func calculateGamePointsPartTwo(row string) int {
	game := findGame(record{determineShape(rune(row[0])), empty, determineEndOfGame(rune(row[2]))})
	return game.b.v + game.aLtB.v
}

// endregion

func main() {
	input, _ := os.ReadFile("in")
	split := strings.Split(string(input), "\n")

	sum := 0
	for _, row := range split {
		//sum += calculateGamePointsPartOne(row)
		sum += calculateGamePointsPartTwo(row)
	}

	println(sum)
}
