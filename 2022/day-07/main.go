package main

import (
	"fmt"
	"os"
	"strings"
)

type node struct {
	name     string
	size     int
	dir      bool
	parent   *node
	children []*node
}

const (
	partOneSmallerThan   = 100000
	partTwoRequiredSize  = 30000000
	partTwoTotalDiskSize = 70000000
)

func readLines(root *node, lines []string) {
	currentNode := root
	for _, line := range lines {
		if '$' == line[0] {
			if "$ ls" == line {
				continue
			}

			var dirName string
			_, _ = fmt.Sscanf(line, "$ cd %s", &dirName)

			if ".." == dirName {
				currentNode = currentNode.parent
			} else {
				for _, child := range currentNode.children {
					if child.name == dirName && child.dir {
						currentNode = child
						break
					}
				}
			}

		} else {
			name := ""
			size := 0
			dir := 'd' == line[0]
			var children []*node
			if dir {
				_, _ = fmt.Sscanf(line, "dir %s", &name)
				children = []*node{}
			} else {
				_, _ = fmt.Sscanf(line, "%d %s", &size, &name)
				children = nil
			}
			currentNode.children = append(currentNode.children, &node{name, size, dir, currentNode, children})
		}
	}
}

func (n *node) sumOfMaxDirSizes(maxSize int) (size int, resSize int) {
	if !n.dir {
		return n.size, 0
	}

	for _, child := range n.children {
		s, c := child.sumOfMaxDirSizes(maxSize)
		size += s
		resSize += c
	}

	n.size = size
	if size <= maxSize {
		resSize += size
	}

	return size, resSize
}

func (n *node) findMinAbove(minSize int) (resSize int) {
	if !n.dir {
		return 0
	}

	for _, child := range n.children {
		if res := child.findMinAbove(minSize); res >= minSize && (resSize == 0 || res < resSize) {
			resSize = res
		}
	}

	if n.dir && n.size >= minSize && (resSize == 0 || n.size < resSize) {
		return n.size
	}
	return resSize
}

func main() {
	input, _ := os.ReadFile("in")
	lines := strings.Split(string(input), "\n")
	lines = lines[1:]

	root := node{"", 0, true, nil, []*node{}}
	readLines(&root, lines)

	totalSize, countSize := root.sumOfMaxDirSizes(partOneSmallerThan)

	println("totalSize     ", totalSize)
	println("countSize     ", countSize)

	freeSpace := partTwoTotalDiskSize - totalSize
	requiredSize := partTwoRequiredSize - freeSpace

	println("freeSpace     ", freeSpace)
	println("requiredSize  ", requiredSize)

	minFolderSize := root.findMinAbove(requiredSize)

	println("minFolderSize ", minFolderSize)
}
