package main

import (
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	input, _ := os.ReadFile("in")
	split := strings.Split(string(input), "\n")

	parseRegex := regexp.MustCompile("[-,]")

	count := 0
	for _, row := range split {
		d := parseRegex.Split(row, -1)
		a, _ := strconv.Atoi(d[0])
		b, _ := strconv.Atoi(d[1])
		x, _ := strconv.Atoi(d[2])
		y, _ := strconv.Atoi(d[3])

		// region Part One
		//if x <= a && b <= y || a <= x && b <= y {
		//	count++
		//}
		// endregion

		// region Part Two
		if !(b < x || y < a) {
			count++
		}
		// endregion
	}

	println(count)
}
